MOVED
===========

Maintenance of this project has moved to the [ROS 2 Safety Working Group](https://github.com/ros-safety):

[https://github.com/ros-safety/contracts\_lite\_vendor](https://github.com/ros-safety/contracts_lite_vendor)
